[//]: # (To preview markdown file in Emacs type C-c C-c p)

# CHARMEC S&C methodology documentation

Documentation of CHARMEC project TS17 *Optimization of material in track switches*.

## How to use

### Reading the documentation

The documentation is hosted by Read the Docs and can be read [online](https://charmec-snc-methodology-doc.readthedocs.io/).

### Building updates to documentation

The documentation is built using **MkDocs**, so most of it is written in Markdown.
To make changes to the documentation, you will need to:

1. Clone the repository
1. Install MkDocs on your computer
1. Update the source files
1. Build the new version by running `mkdocs build` from the top level directory of the project.
1. Open the file `html_docs/worker/index.html` to preview.


> NOTE: If you want to update the documentation at Read the Docs, you need to have admin rights to this repository. If you do, every `push` to the repository will trigger a webhook that builds the new version at Read the Docs.
