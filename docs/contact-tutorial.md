[//]: # (To preview markdown file in Emacs type C-c C-c p)

# How to use `contact` module

## Prerequisites

- Source code of the [module](https://bitbucket.org/ts17/metamodel/)
- [Git](https://git-scm.com)
- [Python 2](https://www.python.org/downloads/release/python-2718/)
- [Abaqus](https://www.3ds.com/products-services/simulia/products/abaqus/) (if you plan to calibrate the metamodel)
- Results from the [previous tutorial](dynamics-tutorial.md)

## Setup
Having obtained a copy of the source code (either via `git clone` of the repository on Bitbucket or by copying from the CHARMEC disc), do the following:

1. Open terminal and navigate to the Git repository (by default, named `metamodel`).
1. Run `git checkout test-example` command. It will set the repository into appropriate state for demonstration purposes.
*You can go back to the original state by checking out one of the branches (e.g. `master` or `VSM_paper`).*
1. Copy all the `metamodel_input_sec<number>` directories generated in the [previous tutorial](dynamics-tutorial.md) into the current (`metamodel`) directory.
1. We will need to provide the wheel and rail geometries. To this end, export the following environmental variables containing paths to the geometry files:

        :::bash
        export dynamics_path=<absolute/path/to/files/in/load_collective> # path to the `dynamics` module (see previous tutorial).
        export profile_path=$dynamics_path/ExampleVehicle/database/wheel_rail_profiles/60E1_v2/Crossing/
        export wheel_profile_path=$dynamics_path/ExampleVehicle/database/wheel_rail_profiles/WheelProfiles/


## Calibrating the metamodel

If metamodel has already been calibrated, you can jump to the [next section](#run).
If it has not, it will be our next step.
*In this demo, we will use the default `R350HT` material. If you would like to change the material from `R350HT` to `Mn13`, uncomment the appropriate section in `create_materials()` function of `preprocess_fun.py` and comment out the one of `R350HT`.*
The calibration is assumed to be done using a computatinal cluster.

2. Open the file `makecontact_simpack.py` that is located in the current directory.
2. Edit line 43 by replacing the e-mail `<user>@chalmers.se` address with your email address. *This is needed to be notified about the completion of all the Abaqus jobs with reference solutions.* 
2. Navigate to the definition of the function `main()` on line 152. There, you need to adjust some of the variables replacing text encompassed in angular brackets `<>` with appropriate values:

        :::python
        section = '700' # select the section number (in this test example we will take 700)
        storage_path = '<path/to/directory/that/will/store/results>'
        snic_project = '<name/of/the/SNIC/project>'

2. Run this script, by executing `python makecontact_simpack.py` in the terminal, to produce shell scripts with batch jobs.
As a result, the directory called `shell_scripts_test_example_sec700` should appear in the current directory.
2. Navigate to `shell_scripts_test_example_sec700` and run `submitjobarray.sh` by e.g. executing `sh submitjobarray.sh` command in the terminal. *This submits a batch job with 4 Abaqus simulations, whose results will be saved to* `storage_path` *that you specified above*.
2. After receiving an e-mail about completion of the submitted batch job, it is recommended to navigate to the `storage_path` directory, launch Abaqus GUI (e.g. via ThinLinc) and examine the results of each of the simultaions (4 in our case). *The e-mail will report on the state of the job in its body, it should be* "State: COMPLETED (exit code 0)". The plot of "MISESMAX" output for simulation 1 is shown below:
    ![Max von Mises stress](img/abaqus_results_1.png)
2. Collect the postprocessed data files into `./postprocess_output_sampled/`. To this end, you need to
    - Open `collect_output_jobarray.sh` and set `storage_path` to the value you chose before.
    - Copy this script to `shell_scripts_test_example_sec700` and navigate to that directory.
    - Run the script specifying a directory to collect output in, e.g. `sh collect_output_jobarray.sh abaqus_output/`. *Note the forward slash `/` at the end.* 
    You end up with a directory (in this case `abaqus_output`) containing files that have the following naming pattern: `contact_sec<number1>_<number2>`, where `<number1>` is section number and `<number2>` is contact case number.
    These files store contact data for each contact case: contact patch, max von Mises stress and pressure distribution, e.g. `contact_sec700_1.txt` should contain the following *(units are mm and MPa)*:
            
            semi-axis a, semi-axis b, max pressure, max Mises
            9.021230     3.529085    1539.137451   791.854797
            
            X   ,  pressure:
            0.00000  1504.25854
            0.25000  1497.33801
            ... # more entries
            9.00000    27.76852
            9.25000     0.00000
            
            Y   ,  pressure:
            0.00000  1504.25854
            0.25196  1528.33765
            ...
            3.52415     1.79072
            3.77533     0.00000

    - Return to the repository's root directory (`metamodel` in this example) and run `makemodel_jobarray.py` script, e.g. via `python makemodel_jobarray.py`. Doing so will produce `model_coefficients_test_example.txt`, which contains calibrated (for cross-section 700) parameters for the metamodel of the contact patch:
    
            # Axis 2 		 Pressure
            9.808117e-03   1.798081e+00
            3.878028e-01   9.701768e-01
            
    - Finally, run `calibratemises.py` script to get `metamises_coefficients_test_example.txt` containing calibrated (for cross-section 700) parameters for the metamodel of the maximum von Mises stress:
            
            # Coefficients for section 700
            -1.536875e-01
            1.398746e+00
            6.000059e-01
            -2.183864e-01
            9.338637e-01
            5.999693e-01
            9.998392e-01

        
##  <a name="run"></a> Applying the metamodel
Once the metamodel coefficents (files `model_coefficients_test_example.txt` and `metamises_coefficients_test_example.txt`) are available, you can solve the normal contact for those cases that are stored in all `metamodel_input_sec<number>` directories, where `<number>` is the position of the cross-section in mm (in this example these are cross-section 700, 750, 800 and 850).
To this end, run the `applymetamodel.py` script. This will produce a bunch of `metamodel_output_sec<number>` directories with metamodel responses.

*During execution, you will see a message similar to this*
    
    /// Metamodel radii combination leads to unphysical contact. Set wheel radius to be 110% of rail radius for contactpointdata-wheel1_rail850_cp1.dat.
    /// Metamodel stress < 60% compressive yield stress. Ignoring this load case for contactpointdata-wheel1_rail850_cp1.dat.

*which demonstrates how the metamodel treats cases when the estimated wheel and rail radii lead to an unphysical contact, and when the predicted maximum von Mises stress leads to an elastic problem.* 
    
Each output file starts with `responses` in its name will look similar to this:

    # R_x_w, [mm] 		 R_y_w, [mm] 		 R_x_r, [mm] 		 Force, [N] 		 a, [mm]   		   b, [mm]  			 p0, [MPa] 		 max Mises, [MPa]
    8.746e+01			4.632e+02			7.517e+01			1.049e+05			9.525e+00			1.943e+00			2.595e+03			1.352e+03

where `R_x_w` and `R_y_w` are wheel radii about X and Y axes; `R_x_r` is rail radius; `Force` is applied normal force; `a` and `b` are larger and smaller contact semi-axes; `p0` is the maximum contact pressure and `max Mises` is the maximum von Mises stress.


## Making inputs for [simulations of plastic deformations](./plasticity-tutorial.md)

Finally, the results of metamodel can be assembled for the further use by running the `makeinputs.py` script.
It will create the following files in each of the `metamodel_ouput_sec<number>` directories:

    Axis_data.input
    Depth_MaxMises.input
    Load.input
    Load_tangential.input
    Position_data.input
    SimulationOrder.input

## Handling errors

- Verify that the environment variables you set via `export` do not contain mistakes. *A typical one would be a double slash `//` in the path instead of a single one.*

- If you tried to run one of the Python scripts and received an error similar to this

        Traceback (most recent call last):
          File "makecontact_simpack.py", line 16, in <module>
            import scipy.interpolate as spi
        ImportError: No module named scipy.interpolate
        
    you are missing some of the necessary Python modules. On the [C3SE](https://www.c3se.chalmers.se) cluster, you can

    1. Run `ml spider SciPy-bundle`
    1. Select on of the bundles listed under *Versions* (e.g. `SciPy-bundle/2019.10-Python-2.7.16`), and repeat the previous step with the chosen bundle (e.g. `ml spider SciPy-bundle/2019.10-Python-2.7.16`).
    This will list the modules that need to be loaded before the bundle can be loaded (e.g. `GCC/8.3.0  OpenMPI/3.1.4`).
    1. Run `ml <list/of/space/separated/modules>` where you list all of the modules with the bundle at the end (e.g. `ml GCC/8.3.0  OpenMPI/3.1.4 SciPy-bundle/2019.10-Python-2.7.16`)

    After these steps you should have all the necessary Python modules.
 
- If you get an error `ifort: command not found` when running Abaqus to calibrate the metamodel, try executing `module purge` in terminal before submitting the batch job `submitjobarray.sh`. Doing so should fix the conflict between the modules that you have loaded previously.
