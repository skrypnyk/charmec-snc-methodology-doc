[//]: # (To preview markdown file in Emacs type C-c C-c p)

# How to use `dynamics` module

## Prerequisites

- Source code of the [module](https://bitbucket.org/skrypnyk/load_collective/)
- [Git](https://git-scm.com)
- [Matlab](https://se.mathworks.com/products/matlab.html)
- [Simpack](https://www.3ds.com/products-services/simulia/products/simpack/)

## Setup

Having obtained a copy of the source code (either via `git clone` of the repository on Bitbucket or by copying from the CHARMEC disc), do the following:

1. Open terminal and navigate to the Git repository (by default, named `load_collective`).
1. Run `git checkout test-example` command. It will set the repository into appropriate state for demonstration purposes.
*You can go back to the original state by checking out one of the branches (e.g. `master` or `VSM_paper`).*

    If you open `./Matlab/MasterVSM.m` at this point, you will see that the script requires some environment variables to be set up.
    To do so, continue in the terminal:

1. `export simpack_location=<path/to/simpack-slv>`, where `<path/to/simpack-slv>` is path to Simpack installation directory.
Under Linux, the path can be obtained by running `which simpack-slv` command (on [C3SE](https://www.c3se.chalmers.se) cluster, you would have to load Simpack module first via `ml Simpack`).
1. `export model_location=<path/to/repo>/ExampleVehicle/main_model/`, where `<path/to/repo>` is absolute path to this repository.
*If in terminal the repository is your current working directory, you can populate the variable via ``export model_location=`pwd`/ExampleVehicle/main_model/``.*
1. And so on:

```bash
export parameter_location=`pwd`/ExampleVehicle/database/mbs_db_ip/
export parameter_file=BenchmarkParameters.subvar
export friction_coef=0.3
export date_stamp=`date +"%Y-%m-%d"`

# And the same for `./Matlab/preparepForMetamodel.m`:

export theor_crossing_point=96.7
export section_start=690
export section_finish=810
export geometry_scaling_factor=1 # Used to scale turnout geometry.
```

## Running the simulations

After setting up the environment variables, you are ready to start the simulations (*on C3SE cluster, you might need to load Matlab first via `ml MATLAB`*):
```bash
cd <path/to/repo>/Matlab
matlab -nodesktop -nosplash -r "try, MasterVSM; catch e, disp(getReport(e,'extended')), exit(1), end, quit"
```
Thereafter comes lots of output to the screen, which ends in
```bash
INFO: Creating file "<path/to/repo>/ExampleVehicle/main_model/SimpackResults/TRV_Crossing_Thru_EslovTM_Fs_10000_f035_vsm_2.mat"...  
INFO: Creating file "<path/to/repo>/ExampleVehicle/main_model/SimpackResults/TRV_Crossing_Thru_EslovTM_Fs_10000_f035_vsm_2.sbr"...  
INFO: Compressing SBR ...  
INFO: successfully finished.
```

This will create directories `./Matlab/metamodel_input_sec<number>` for each rail cross-section in between `section_start` and `section_finish`.
Each directory contains contact point data that serves as input to the metamodel of wheel--rail contact (see [contact module](contact-tutorial.md)).
In addition, `SIMPACK_<date_stamp>_loadcase_{1,2}.mat` will be created.
These files contain input for [wear module](wear-tutorial.md).

To check that Simpack result files contain correct data, you can follow these steps:

1. Start Simpack postprocessor by running `simpack-post` in terminal.

1. In the menu panel, press **Open** and navigate to the result files under `./ExampleVehicle/main_model/SimpackResults/`. Open the first file as shown:  
![simpack result files](img/dynamics-open_simpack_results.png)

1. Expand the **Result Tree** and select *Q-force* as shown:
![select Q-force](img/dynamics-select_Q_force.png)

1. Drag and drop the selected item to the blank space in the **Page** area. This action should result in the following plot:
![Q-force plot](img/dynamics-Q_force_plot.png)

1. If you double click the x-axis labels and in the **Axis** tab set **Scaling** to [1.65 1.8], you should see a zoomed-in plot:
![Q-force plot zoomed](img/dynamics-Q_force_plot_zoom.png)

    Additionally, you can check the lateral contact position on rail by right-clicking **Pageset** entry in the **Session Tree** on the left and pressing **Add Page**.
1. Select lateral position on rail in **Result tree**:  
![select Y position](img/dynamics-select_yr_position.png)

1. Drag and drop it to the new blank **Page** area. You should see the following plot:
![lateral position plot](img/dynamics-yr_plot.png)

1. Zooming-in to the plot gives
![lateral position plot zoom](img/dynamics-yr_plot_zoom.png)


## Handling errors

- Verify that the environment variables you set via `export` do not contain mistakes. *A typical one would be a double slash `//` in the path instead of a single one.*
