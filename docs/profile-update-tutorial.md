[//]: # (To preview markdown file in Emacs type C-c C-c p)

# How to use `profile update` module

## Prerequisites

- Source code of the [module](https://bitbucket.org/ts17/sum_damage/)
- [Git](https://git-scm.com)
- [Matlab](https://se.mathworks.com/products/matlab.html)
- Results from the [contact](contact-tutorial.md),
  [plasticity](plasticity-tutorial.md) and the [wear](wear-tutorial.md) tutorials.

## Setup

Having obtained a copy of the source code (either via `git clone` of the repository on Bitbucket or by copying from the CHARMEC disc), do the following:

1. Open terminal and navigate to the Git repository (by default, named `sum_damage`).
1. Run `git checkout test-example` command. It will set the repository into appropriate state for demonstration purposes.
*You can go back to the original state by checking out one of the branches (e.g. `master` or `VSM_paper`).*

    If you open `smoothen_damage.m` and examine it, you will see that the script relies on a couple of environment variables.
    To set them, execute the following in the terminal:
    
        :::bash
        export N_sequences=2 # Number of load sequences == scaling for wear.
        export geometry_scaling_factor=1 # Used to scale turnout geometry.
        
        
## Running the update

After setting up the environment variables, you are ready to start the simulations (*on C3SE cluster, you might need to load Matlab first via `ml MATLAB`*):

```bash
cd <path/to/repo/>
matlab -nodesktop -nosplash -nojvm -r "try, smoothen_damage('sec700'); catch e, disp(getReport(e,'extended')), exit(1), end, quit"
```

This should produce `new_profile_sec700.dat` file in the root directory of the repository.
The file contains the superimposed geometry change from simulations of wear and plastic deformation.


## Preparing for the next methodology iteration

The new rail profiles need to be copied into the Simpack model's geometry directory before the next iteration of the methodology.
This can be achieved by running the `update_geometry_simpack.py` script.
It relies on some environment variables.
To set them, run these commands in terminal:

```bash
export dynamics_path=<absolute/path/to/files/in/load_collective> # path to the `dynamics` module (see its tutorial).
export profile_path=$dynamics_path/ExampleVehicle/database/wheel_rail_profiles/60E1_v2/Crossing/
```

Now you should be ready to run the script:

```python
python update_geometry_simpack.py new_profile_sec700.dat $profile_path
```


## Visualising results

The repository contains the `postprocess` directory with several plotting routines.
The two most interesting are probably

- `plot_area_vs_traffic.py` that plots into files wear and shape change areas over traffic and length of the crossing.
Here is an example taken from one of our [publications](index.md):
![shape change example](img/shape_change_example.png)

- `plot_deformation_3d.py` that plots the crossing in 3D with normal surface change represented by colour.
Here is an example taken from one of our [publications](index.md):
![3d surface change example](img/deformation_3d_example.png)


## Handling errors

- Verify that the environment variables you set via `export` do not contain mistakes. *A typical one would be a double slash `//` in the path instead of a single one.*
