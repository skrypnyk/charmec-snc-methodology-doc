[//]: # (To preview markdown file in Emacs type C-c C-c p)

# Bringing it all together

## Prerequisites

- Source code of the [module](https://bitbucket.org/ts17/methodology)
- [Git](https://git-scm.com)
- [Bash shell](https://www.gnu.org/software/bash/)


## Setup

All the five modules ([dynamics](dynamics-tutorial.md), [contact](contact-tutorial.md), [plasticity](plasticity-tutorial.md), [wear](wear-tutorial.md) and [profile update](profile-update-tutorial.md)) are controlled by a Bash script `run_methodology.sh` that can be found in the Bitbucket repository called **methodology**.

### If copying files from CHARMEC disc

In this case you already have all the modules downloaded. All you need to do is simply execute `git checkout test-example` in the terminal from the root directory (the default name is `methodology`) and each of the five subdirectories (modules).

### If cloning from Bitbucket

The modules are incorporated in this repository as *Git submodules*, which makes it easy to get all of the pieces of the puzzle together.
A short explanation of how to use submodules can be found [here](https://gist.github.com/bytebunny/d062fdb039e659fbacbdc07cfb685ea1).
To clone the repository from Bitbucket, together with the submodules (and their submodules), execute the following commands in the terminal:

1. Clone the main repository and register submodules:

        :::bash
        git clone --recurse-submodules https://<user-name>@bitbucket.org/ts17/methodology.git # replace <user-name> with your user name at Bitbucket.

1. Enter your Bitbucket password when prompted. This will download the main repository, but end with an error message like this:

        :::bash
        Submodule 'contact' (git@bitbucket.org:ts17/metamodel.git) registered for path 'contact'
        Submodule 'dynamics' (git@bitbucket.org:skrypnyk/load_collective.git) registered for path 'dynamics'
        Submodule 'plasticity' (git@bitbucket.org:ts17/plasticity.git) registered for path 'plasticity'
        Submodule 'profile_update' (git@bitbucket.org:ts17/sum_damage.git) registered for path 'profile_update'
        Submodule 'wear' (git@bitbucket.org:ts17/wear_rcf.git) registered for path 'wear'
        Cloning into 'contact'...
        Permission denied (publickey).
        fatal: Could not read from remote repository.

    This is because the submodules have SSH urls that are not connected to any SSH keys, because we have not set up any.
    The easiest is to update those links with HTTPS links, which are *personal* (with your account name).

1. To fix the submodule links, execute

        :::bash
        cd methodology
        git config submodule.dynamics.url https://<user-name>@bitbucket.org/skrypnyk/load_collective.git # replace <user-name> with your user name at Bitbucket.
        git config submodule.contact.url https://<user-name>@bitbucket.org/ts17/metamodel.git # replace <user-name> with your user name at Bitbucket.
        git config submodule.plasticity.url https://<user-name>@bitbucket.org/ts17/plasticity.git # replace <user-name> with your user name at Bitbucket.
        git config submodule.wear.url https://<user-name>@bitbucket.org/ts17/wear_rcf.git # replace <user-name> with your user name at Bitbucket.
        git config submodule.profile_update.url https://<user-name>@bitbucket.org/ts17/sum_damage.git # replace <user-name> with your user name at Bitbucket.
        
1. Execute `git submodule update`. This will start downloading the submodules and prompting you for pasword for each of them.

1. Lastly, repeat the steps for the meshing submodule in plasticity:

        :::bash
        cd plasticity
        git config submodule.2d_meshing.url https://<user-name>@bitbucket.org/ts17/2d_meshing.git # replace <user-name> with your user name at Bitbucket.
        git submodule update
        
    Enter the password one more time.

After the downloads are completed, go to the root repository (`methodology`) and run `git checkout test-example` command.
It will set the repository into appropriate state for demonstration purposes.
Repeat the command in each of the five subdirectories.
*You can go back to the original state by checking out one of the branches (e.g. `master` or `VSM_paper`) or the `ts17_final` tag.*

### Compile plasticity program and add it to path

This step is necessary regardless where you obtained the source code from.
Shortly we will submit a batch job to run on a computational node (computer), and for that node to be able to execute the plasticity calculation, the environtment variable `PATH` should contain the path to the compiled plasticity program.

Compile the plasticity program outside of this repository's directory tree.
*For example, if this repository is under `~/methodology/`, compile the plasticity under `~/plasticity/`, so that "plasticity" is not a subdirectory of "methodology".*
It is not a problem if it is built in the directory tree, but then the binaries will be copied around for no good reason.
The details on how to compile the program are in the [plasticity tutorial](plasticity-tutorial.md).

To add the compiled program to the `PATH`, add the following line to your `~/.bash_profile` file (create one if not present):
```bash
PATH=$PATH:~/path/to/plasticity/build/
```


## Learning the gears

Now that you have the source code, you are almost ready to run a test simulation.
All you need to do is 

1. Get access to one of the C3SE clusters, e.g. [Hebbe](https://www.c3se.chalmers.se/about/Hebbe/).
2. Open `run_methodology.sh` in the text editor of your choice and set your account details at the top of the file, replacing the text inside `<>` with the appropriate content:

        :::bash
        #SBATCH -A <project> # name of the SNIC project.
        #SBATCH -p <partition> # name of the partition.
        #SBATCH --mail-user <your-email> # email to receive notification about the end of the batch job.
        
    and the path to your storage project on line 52: 
    
        :::bash
        snic_storage=<path/to/your/storage> # probably starts with /cephyr/...

At this point you should be ready to run the script (batch job):
```bash
sbatch -J test -o test_example.out run_methodology.sh
```
This will submit a batch job to [Slurm](https://slurm.schedmd.com/documentation.html) (workload manager used by C3SE) with the name "test" and channel the outputs intended for the standard output (screen) to the file called in this case "test_example.out".

!!! Note
    In this tutorial we relied on the metamodel coefficients calibrated for the demo in the contact module.
    Do not forget to recalibrate the metamodel for your wheel and rail geometries, and a new material before starting using the methodology for reasearch purposes.

### Settings

About the first 90 lines of `run_methodology.sh` are the different settings for the simulations that are either passed down to the submodules or need to be in sync with them for the whole schtick to work properly.
We will now go through some of them to give you an idea about their purpose.

- `logfile=$TMPDIR/run_methodology.log` is the file where the script messages will be collected. The messages signal about the progress of the simulation and can be used whe troubleshooting (see [Handling errors](#errors) for more details).

- `simpack_initial_state` provides an environment variable to use in a conditional statement inside the dynamics module and call Simpack with or without *initial state* files (consult Simpack's Documentation for details).

- `crossing_angle` is a local variable to set the environment variable `geometry_scaling_factor`, which controls how the spacing between rail profiles should be scaled.
*Different turnout geometries were obtained in our studies using the same rail profiles but different spacing between them.*

- `section_start` and `section_finish` denote the interval in mm for the rail cross-sections to be analysed.

- `sample_size` should have the same value as the number of different wheels used in the dynamics module, because it is read by other modules.

- `N_sequences` tells the plasticity and wear modules how many times the load sequence needs to be repeated.

- `N_iterations` number of iterations of the methodology.


## Verifying the setup

Once you get an email from Slurm with the status "completed", you can navigate to the directory you assigned to `snic_storage` (see above) and see the results of simulations together with the backup files (for troubleshooting and postprocessing purposes).
The directory tree should look like [this](img/tree_test_example.html) and all the files are listed [here](img/tree_test_example_detailed.html).


## Restarting the simulations

The time will come when the simulations finish (successfully or otherwise) and you would like to restart them to build up on the progress you have made.

### After failure

1. Examine the end of `.out` file that was created after batch job submission (`test_example.out` in the example above). 
It should specify the error message that triggered the failure.
1. The `run_methodology.sh` copies the entire file tree in its state during failure into your storage space (specified in `out_dir` variable in the script).
Navigate to that directory and look for the directory `iteration_<number>` with the highest `<number>`.
Inside, you will find all the files.
You can examine and troubleshoot the code on the spot because all the input data should be present as well.
1. Assuming you have fixed the issue, delete the `iteration_<number>` directory and set `method_iteration` variable inside `run_methodology.sh` to the value of `<number>`.
1. If the failure occured not in the first methodology iteration, the material state may not be undeformed anymore, and you probably want to communicate to the plasticity calculation that the state variables need to be read.
This is why you need to set `virgin_material` to 0 inside `run_methodology.sh`.
1. Proceed with the steps in the section on restarting after completion.

### After completion

2. If you have not updated `virgin_material` inside `run_methodology.sh` yet, this is a good time to do it.
The `virgin_material` tells the plasticity calculation whether the state variables have non-zero values that need to be read from a file, set it to 0.
2. Copy the plasticity files for each cross-section to the repository. For this, navigate in the terminal to the directory `iteration_<number>/plasticity/` in your storage space with the highest `<number>` (assuming you have already deleted the directory with the highest number in case of failure) and execute the following command:

        :::bash
        cp -r sec* <path/to/methodology/plasticity/> # Replace <...> with the actual path.

2. Before updating the rail profiles in the repository, set up the environment variables `dynamics_path`, `profile_path` replacing `<>` with the actual values:

        :::bash
        export dynamics_path=<absolute/path/to/files/in/load_collective/> # path to the `dynamics` module without the trailing slash (/) (see its tutorial).
        export profile_path=<path/to/rail/geometry/files/> # path (with the trailing slash) to files with crossing rail geometries, usually .prr files.
        
    These variables are used by `update_geometry_simpack.py` (see the next step).
        
2. Copy the `profile_update/update_geometry_simpack.py` to the directory with the latest rail geometries: `iteration_<highest_number>/profile_update/` and navigate there in terminal.
2. Execute the following command replacing `<>` with numbers:

        :::bash
        for i in {<lowest_number>..<highest_number>..<increment>}; do python update_geometry_simpack.py new_profile_sec${i}.dat $profile_path; done
        
    This loop iterates over a range of values with the step. 
    For instance, if in your directory there are profiles with positions starting from, let's say, 390 up to 700 with the increment of 10, then you write `for i in {390..700..10}; ...` in your loop.
    You might need to repeat the command changing the range and the step, should you have varying spacing between profiles, e.g. not only 10, but also 50.
    Or you can simply specify the biggest range with the smallest step and watch the errors popping up that the files cannot be found.
    
2. And you are done! Now you can run the `sbatch ...` command to submit a new batch job.


## <a name="errors"></a> Handling errors

- If you get an error when cloning the repository check that you have the necessary permission (see [Contact information](index.md#contact-info)).
Try also reading [these instructions](https://gist.github.com/bytebunny/d062fdb039e659fbacbdc07cfb685ea1).
- Verify that the environment variables you set via `export` do not contain mistakes. *A typical one would be a double slash `//` in the path instead of a single one.*
- The shell script loads a couple of Slurm modules that can become outdated over time. So if you get an error regarding an unknown module, you might need to replace those with the more recent ones, *cross your fingers and pray that it still works*.
- If the batch job fails, the possible clues can be found in the output file you specified together with `sbatch` command (`test_example.out` in this tutorial) and in the `run_methodology.log` that should be present under `snic_storage` path (see above) inside the `iteration_<number>` directory with the largest `<number>`.
