[//]: # (To preview markdown file in Emacs type C-c C-c p)

# Simulation methodology

This documentation describes a multidisciplinary iterative simulation methodology for predicting long-term damage in railway Switches & Crossings (S&C) developed within project [TS17](https://research.chalmers.se/en/project/?id=9309) at Chalmers Railway Mechanics ([CHARMEC](http://www.charmec.chalmers.se)) research centre.

The methodology consists of four modules:

1. [Simulation of dynamic vehicle&ndash;track interaction](#dynamics)
1. [Simulation of wheel&ndash;rail normal contact](#contact)
1. [Simulation of damage evolution](#damage) (wear and plastic deformation)
1. [Profile update](#profile-update)

![Methodology](img/methodology.png)

## <a name="dynamics"></a>1. Dynamic vehicle&ndash;track interaction

Every iteration of the methodology begins with this module.
Here, Simpack is used to simulate a rail vehicle passing a turnout.
A number of simulations is carried out, where each vehicle is assigned a wheel profiles, a speed and a wheel&ndash;rail friction coefficient.
The simulations produce input for simulations of [contact](#contact) and [damage](#damage).

More details about this module and instructions on how to use it are given in [this](dynamics-tutorial.md) tutorial.

## <a name="contact"></a>2. Wheel&ndash;rail normal contact

This is the second module of the methodology. 
Here, for every contact scenario at the selected cross-sections, a normal contact problem is solved using a Hertzian-based metamodel (surrogate model) that was calibrated against finite element simulations with physically non-linear material model.
This module computes contact patch sizes and maximum von Mises stresses for the simulation of [plastic deformation](#plastic-deformation).

More details about this module and instructions on how to use it are given in [this](contact-tutorial.md) tutorial.

## <a name="damage"></a>3. Damage evolution

The third module of the methodology consists of two submodules, carried out independently:

1. [Simulation of accumulated plastic deformation](#plastic-deformation)
1. [Simulation of wear](#wear)

### <a name="plastic-deformation"></a>3a. Plastic deformation

Having the positions of contact, and normal and tangential forces from [the first module](#dynamics), as well as the lateral semi-axes of the contact patches from [the second module](#contact), simulations of accumulated plastic deformation are performed for the selected cross-sections, i.e. in 2D, to reduce the computational effort, using in-house finite element program.
To convert the results of the 3D contact simulations to 2D, the normal force is adjusted such that the same levels of maximum
von Mises stress in each cross-section are reached, as those provided by [the second module](#contact).

More details about this module and instructions on how to use it are given in [this](plasticity-tutorial.md) tutorial.

### <a name="wear"></a>3b. Wear

Having the positions of contact, forces and creepages from [the first module](#dynamics), the wear depth is computed for the crossing, but only around the selected cross-sections to reduce the computational effort. The simulations are based on FASTSIM and employ Archard's model for sliding wear.

More details about this module and instructions on how to use it are given in [this](wear-tutorial.md) tutorial.

## <a name="profile-update"></a>4. Profile update

The last module of the methodology is superposition of profile change, predicted by [the third module](#damage).
The rail profiles are updated for the next iteration of the methodology (see [module 1](#dynamics)).

More details about this module and instructions on how to use it are given in [this](profile-update-tutorial.md) tutorial.

# Related publications

The details on the methodology in its state at the time of writing can be found in the following articles:

1. [Metamodelling of wheel&ndash;rail normal contact in railway crossings with elasto-plastic material behaviour](https://doi.org/10.1007/s00366-018-0589-3). *Engineering with Computers*, Vol. 35 (2019), pp. 139&ndash;155.
2. [Prediction of plastic deformation and wear in railway crossings &mdash; Comparing the performance of two rail steel grades](https://doi.org/10.1016/j.wear.2019.03.019). *Wear*, Vol. 428-429 (2019), pp. 302&ndash;314.
3. [Long-term rail profile damage in a railway crossing: Field measurements and numerical simulations](https://doi.org/10.1016/j.wear.2020.203331). *Wear*, in press.
4. [On the influence of crossing angle on long-term rail damage evolution in railway crossings](https://doi.org/10.1080/23248378.2020.1864794). *International Journal of Rail Transportation*, in press.

# <a name="contact-info"></a> Contact information

Should you have any questions, please contact Professor Magnus Ekh at the division of Material and Computational Mechanics at Chalmers: `magnus.ekh@chalmers.se`.
