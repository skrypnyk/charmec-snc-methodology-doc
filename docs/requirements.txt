# Read the Docs recommends to pin the MkDocs version used for your project to build the docs to avoid potential future incompatibilities.
mkdocs>=1.1.2
