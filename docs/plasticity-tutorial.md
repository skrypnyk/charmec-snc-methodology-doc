[//]: # (To preview markdown file in Emacs type C-c C-c p)

# How to use `plasticity` module

## Prerequisites

- Source code of the [module](https://bitbucket.org/ts17/plasticity/)
- [Git](https://git-scm.com)
- Fortran compiler
- [BLAS](http://www.netlib.org/blas/)
- [LAPACK](http://www.netlib.org/lapack/)
- [PARDISO](http://www.pardiso-project.org)
- [MATLAB](https://se.mathworks.com/products/matlab.html) +
  [CALFEM](https://sourceforge.net/projects/calfem/) (for mesh generation and plotting)
- [Cmake](https://cmake.org)
- Results from the [previous tutorial](contact-tutorial.md)

## Setup

Having obtained a copy of the source code (either via `git clone` of the repository on Bitbucket or by copying from the CHARMEC disc), do the following:

1. Open terminal and navigate to the Git repository (by default, named `plasticity`).
1. The meshing module is provided as a [Git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules), so unless you copied the code from the CHARMEC disc, a few extra steps are needed:
    - Run `git submodule init` from the `plasticity` directory.
    - Run `git submodule update`.
    This should download the source code, but will put the repository in the "detached HEAD" state.
    *It is not important, unless you plan to make changes to the code, in which case go to the `2d_meshing` directory and check out the master branch: `git checkout master`.*
1. Run `git checkout test-example` command. It will set the repository into appropriate state for demonstration purposes.
*You can go back to the original state by checking out one of the branches (e.g. `master` or `VSM_paper`).*


## Building executable

The following steps will create executable `plasticity` in the directory of your choice (`sec700` in this expample).

### Locally
1. `cd sec700`
1. `cmake ../src`
1. `make`

### On the cluster (Hebbe at C3SE)

1. Copy this repository to the cluster
1. `cd <path/to/this/repo/on/cluster>`
1. `sh ./build_on_hebbe.sh <build_dir>`


!!! Note
    The path to `plasticity` executable you have just built has to be present in the environment variable `PATH` if you do not want to copy this executable around (on the compute node, for example).
    This is why, it is recommeded that you add `export PATH=$PATH:<path/to/build_dir/>` to your `~/.bashrc` file, where `<...>` needs to be replaced with the actual path.
    

## Generating mesh

Before we run the analysis of plastic deformation, we need to create the finite element mesh for those profiles we would like to consider.

2. Navigate to directory `2d_meshing` and create there directories called `input` and `output`.
2. Copy rail geometry files you created in the [previous tutorial](contact-tutorial.md) (from `metamodel_ouput_sec<number>`) to the `input` directory.
In this example we'll consider section `700`: `cp ../../metamodel/metamodel_output_sec700/rail_profile*.rail input/`
2. Run the meshing script: 

        :::bash
        matlab -nodesktop -nosplash -nojvm -r "try, gmesh; catch e, disp(getReport(e,'extended')), exit(1), end, quit"

    This should have created the mesh file `output/rail_profile_700.grid`.
    

## Running analysis

3. Copy input files from directory `input_<material>` to `sec700`, where `<material>` is one of the steel grades for which a material model has been calibrated (currently **R350HT** and **Mn13**). The example will use **R350HT**.
3. Copy the input files for plasticity generated in the [previous tutorial](contact-tutorial.md) to `sec700`.
In this example, we will consider section `700`: `cp ../metamodel/metamodel_output_sec700/*.input sec700`.
3. The main input file `sec700/Innotrack.input` needs to be edited to be in sync with the rest of the input files in this example:
    - Set number of cycles `ncycles` and `ncycles_tot` to 4.
    - Set number of sequences `nseq` to 2.
3. Move and rename the mesh file to the `sec700` directory: `mv output/rail_profile_700.grid ../sec700/Basegrid.grid`.
3. Run the `plasticity` executable located in the `sec700` directory. You should see the following output:
   
        /// 3d->2d results are available. Starting actual plasticity calculation.
        Saving data for sequence           1
        Saving data for sequence           2

    The output above means that, at first, the load in the input files has been adjusted such that the maximum von Mises stress reaches the levels predicted by the metamodel (and provided in `Depth_MaxMises.input`). Then 2 load sequences have been simulated, where each sequence consists of 4 load cycles.
    

## Visualising results

The repository contains directory `plotting` that has a number of plotting scripts.
For example, start Matlab with GUI and run `plotting/plot_mesh.m`.
It will create two plots:

- Deformed and undeformed mesh
![Mesh](img/mesh.png)

- Accumulated plastic strain
![Plastic strain](img/accum_plast_strain.png)


## Handling errors

- If you have problems downloading the meshing module via Git, it might be the permission issue.
Try reading [these instructions](https://gist.github.com/bytebunny/d062fdb039e659fbacbdc07cfb685ea1).
