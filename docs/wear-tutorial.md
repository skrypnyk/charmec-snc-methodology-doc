[//]: # (To preview markdown file in Emacs type C-c C-c p)

# How to use `wear` module

## Prerequisites

- Source code of the [module](https://bitbucket.org/ts17/wear_rcf/)
- [Matlab](https://se.mathworks.com/products/matlab.html)
- Results from the [previous tutorial](dynamics-tutorial.md)


## Setup

Having obtained a copy of the source code (either via `git clone` of the repository on Bitbucket or by copying from the CHARMEC disc), do the following:

1. Open terminal and navigate to the Git repository (by default, named `wear_rcf`).
1. Run `git checkout test-example` command. It will set the repository into appropriate state for demonstration purposes.
*You can go back to the original state by checking out one of the branches (e.g. `master` or `VSM_paper`).*

    If you open `main.m` and the function it calls at this point, you will see that the script requires some environment variables to be set up.
    To do so, continue in the terminal:

        :::bash
        export material=R350HT
        export sample_size=2 # Because we selected two wheel profiles in simulations of dynamic vehicle-track interaction.
        export date_stamp=`date +"%Y-%m-%d"` # Has to be the same date as in the `dynamics` module (see its tutorial). Format: <year>-<month_number>-<day>
        export section_numbers=700 # We consider only one rail cross-section
        export geometry_scaling_factor=1 # Used to scale turnout geometry.
        export section_start=650 # Needs to be smaller than the first considered rail cross-section.
        export section_finish=750 # Needs to be larger than the last considered rail cross-section.


## Running the simulations

1. Create a directory called `SIMPACK_RESULTS` inside `INPUT`.
1. Copy the `.mat` files with Simpack results that you obtained in the [previous tutorial](dynamics-tutorial.md).
Assuming your current working directory is `wear_rcf` and it is in the same level with the `dynamics` module repository, you can run in terminal

        :::bash
        cp ../load_collective/Matlab/SIMPACK_*.mat INPUT/SIMPACK_RESULTS
        
     This will copy two files, since in the [previous tutorial](dynamics-tutorial.md) we simulated two vehicles with unique wheel profiles.

1. Prepare rail geometry files for reading:
   
    - Export additional environment variables:
   
            :::bash
            export dynamics_path=<absolute/path/to/files/in/load_collective> # path to the `dynamics` module (see its tutorial).
            export profile_path=$dynamics_path/ExampleVehicle/database/wheel_rail_profiles/60E1_v2/Crossing/
            export left_rail=True # Coordinate transformation is needed.
        
    - Run the `get_geometry.py` script:
   
            :::python
            python get_geometry.py
        
        This will create a number of files under `INPUT/PROFILES/crossing_profiles` that will be used to create 3D geometry for the simulations of wear.

1. Run the simulations of wear *(don't forget to load Matlab if you are working on the cluster: `ml MATLAB`)*:

        :::bash
        matlab -nodesktop -nosplash -r "try, main; catch e, disp(getReport(e,'extended')), exit(1), end, quit"

    The program should produce the following output:
    
        Load case number: 1 Wheel number: 1
        Load case number: 1 Wheel number: 2
        Load case number: 1 Wheel number: 3
        Load case number: 1 Wheel number: 4
        Load case number: 2 Wheel number: 1
        Load case number: 2 Wheel number: 2
        Load case number: 2 Wheel number: 3
        Load case number: 2 Wheel number: 4

    where the load case number is one of the simulated vehicles with a unique wheel profile.
    In this example, each vehicle has two bogies with two axles.


## Handling errors

- Verify that the environment variables you set via `export` do not contain mistakes. *A typical one would be a double slash `//` in the path instead of a single one.*
